import unittest
from Statki_gra.statki_gra import *
class Test(unittest.TestCase):
    def test_Statki(self):
        s=Statki()
        self.assertEqual(s.getStatki(),[4,3,3,2,2,2,1,1,1,1])
        self.assertEqual(s.getlistlen(),10)
    def test_Faza(self):
        f=Faza()
        self.assertEqual(f.getFaza(),"poczatek")
        f.next()
        self.assertEqual(f.getFaza(),"gra")
        f.next()
        self.assertEqual(f.getFaza(),"koniec")
        f.next()
        self.assertEqual(f.getFaza(),"koniec")



if __name__ == '__main__':
    unittest.main()
