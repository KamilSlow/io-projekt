from tkinter import *
from tkinter import messagebox
import time
import random as rand

class Przycisk_def(object):
    def Funkcja_wybor(self,gra,*inne):
        if gra.getFaza().getFaza()=="poczatek":
            self.Funkcja_faza1(gra,*inne)
        elif gra.getFaza().getFaza()=="gra":
            self.Funkcja_faza2(gra,*inne)
        elif gra.getFaza().getFaza()=="koniec":
            self.Funkcja_faza3(gra,*inne)
        else:
            print("error")
    def Funkcja_faza1(self,gra,*inne):
        pass
    def Funkcja_faza2(self,gra,*inne):
        pass
    def Funkcja_faza3(self,gra,*inne):
        pass

    
class Przycisk(Przycisk_def):
    def __init__(self,gra):
        self._rzad=0
        self._nazwa=" "
        self.pol(gra)
    def pol(self,gra):
        self._kolumna=10
        self._kolor="brown"
        self._pole=Button(gra.getgra(),height=2,width=15,bg=self._kolor,text=self._nazwa,command=lambda:self.Funkcja_wybor(gra))
        self._pole.grid(row=self._rzad,column=self._kolumna)

        
class Plansza(Przycisk_def):
    def __init__(self,gra):
        self._kolumna=0
        self._kolor="gray"
        self.pol(gra)
    def pol(self,gra):
        self._pola_okol=set()
        self._ustawione_statki=set()
        self._zakazane_pola=set()
        self._trafione_statki=set()
        self._rzad=0
        self._liczba_kolumn=10
        self._liczba_rzedow=10
        self._statki=Statki()
        self._statki_it=0
        self._temp_statki=set()
        self._orientacja="pionowa"
        self._r_ogr=self._rzad+self._liczba_rzedow-1
        self._k_ogr=self._kolumna+self._liczba_kolumn-1
        for r in range(self._rzad,self._rzad+self._liczba_rzedow):
            for c in range(self._kolumna,self._kolumna+self._liczba_kolumn):
                self._pole=Button(gra.getgra(),height=2,width=5,bg=self._kolor,command=lambda gra=gra, r=r, c=c:self.Funkcja_wybor(gra,r,c))
                self._pole.bind("<Enter>",lambda event,gra=gra, r=r, c=c:self.podswietl(gra,r,c))
                self._pole.bind("<Leave>",lambda event,gra=gra, r=r, c=c:self.zgas(gra,r,c))
                self._pole.bind("<Button-3>",lambda event,gra=gra,r=r,c=c:self.orient(gra,r,c))
                self._pole.grid(row=r,column=c)
    def zgas(self,gra,r,c):
        pass
    def orient(self,gra,r,c):
        pass
    def podswietl(self,gra,r,c):
        pass
    def okolica(self,r,c):
        if (r,c+1) not in self._zakazane_pola and c+1<self._k_ogr+1:
            self._pola_okol.add((r,c+1))
        if (r,c-1) not in self._zakazane_pola and c-1>self._kolumna-1:
            self._pola_okol.add((r,c-1))
        if (r+1,c) not in self._zakazane_pola and r+1<self._r_ogr+1:
            self._pola_okol.add((r+1,c))
        if (r-1,c) not in self._zakazane_pola and r-1>self._rzad-1:
            self._pola_okol.add((r-1,c))
    def sprawdz_pola(self):
        tmp=self._pola_okol.copy()
        for x in tmp:
            if x in self._zakazane_pola:
                self._pola_okol.remove(x)
    def splonal(self,gra):
        tmp=self._temp_statki.copy()
        for x in tmp:
            if x in self._ustawione_statki:
                (r,c)=x
                if self.czyplonie(r,c)==False:
                    self._temp_statki.remove(x)
                    for y in range(r-1,r+2):
                        for q in range(c-1,c+2):
                            if (y,q) not in self._trafione_statki and y>self._rzad-1 and q>self._kolumna-1 and y<self._r_ogr+1 and q<self._k_ogr+1:
                                self.chybiony(gra,y,q)
    def chybiony(self,gra,r,c):
        self._pole = Button(gra.getgra(),height=2, width=5, bg="blue")
        self._pole.grid(row=r, column=c)
    def trafiony(self,gra,r,c):
        self._pole = Button(gra.getgra(),height=2, width=5, bg="red")
        self._pole.grid(row=r, column=c)
    def czyplonie(self,r,c):
        if (r+1,c) in self._ustawione_statki and (r+1,c) not in self._trafione_statki:
            return True
        if (r-1,c) in self._ustawione_statki and (r-1,c) not in self._trafione_statki:
            return True
        if (r,c+1) in self._ustawione_statki and (r,c+1) not in self._trafione_statki:
            return True
        if (r,c-1) in self._ustawione_statki and (r,c-1) not in self._trafione_statki:
            return True
        else:
            return False
    def czykoniec(self,gra):
        if self._ustawione_statki == self._trafione_statki:
            gra.getFaza().next()
            return True
        else:
            return False

        
class Nowa_Gra(Przycisk):
    def __init__(self,gra):
        self._rzad=0
        self._nazwa="Nowa Gra"
        self.pol(gra)
    def Funkcja_faza1(self,gra,*inne):
        if gra.getStan()==1:
            gra.getFaza().next()
            gra.StatkiSI()
            if rand.randint(0,1)==0:
                messagebox.showinfo("Gra się rozpoczyna", "Zaczyna SI")
                gra.StartSI()
            else:
                messagebox.showinfo("Gra się rozpoczyna", "Zaczyna Użytkownik")
        else:
            messagebox.showinfo("Jeszcze nie wszystko", "Nie zostały rozmieszczone wszystkie statki")
    def Funkcja_faza2(self,gra,*inne):
        self.reset(gra)
    def Funkcja_faza3(self,gra,*inne):
        self.reset(gra)
    def reset(self,gra):
        result=messagebox.askyesno("Nowa Gra","Czy chcesz zacząć grę od nowa?")
        if result == True:
            gra.Reset()

            
class Reset(Przycisk):
    def __init__(self,gra):
        self._rzad=1
        self._nazwa="Reset"
        self.pol(gra)
    def Funkcja_faza1(self,gra,*inne):
        gra.Reset()
    def Funkcja_faza2(self,gra,*inne):
        pass
    def Funkcja_faza3(self,gra,*inne):
        pass

    
class Plansza_SI(Plansza):
    def __init__(self,gra):
        self._kolumna=11
        self._kolor="green"
        self.pol(gra)
    def ustawianie(self):
        self._zakazane_pola.add((-1,-1))
        for ship in self._statki.getStatki():
            r=-1
            c=-1
            while((r,c) in self._zakazane_pola):
                r=rand.randint(self._rzad,self._r_ogr)
                c=rand.randint(self._kolumna,self._k_ogr)
                if(rand.randint(0,1)==0):
                    r2=r+ship-1
                    c2=c
                else:
                    r2=r
                    c2=c+ship-1
                if (r2,c2) in self._zakazane_pola or r2>self._r_ogr or c2>self._k_ogr:
                    r=-1
                    c=-1
            for i in range(r,r2+1):
                for j in range(c,c2+1):
                    self._ustawione_statki.add((i,j))
            for i in range(r-1,r2+2):
                for j in range(c-1,c2+2):
                    self._zakazane_pola.add((i,j))
        self._zakazane_pola=set()
    def Funkcja_faza1(self,gra,*inne):
        pass
    def Funkcja_faza2(self,gra,*inne):
        self._zakazane_pola.add((inne[0],inne[1]))
        if (inne[0],inne[1]) in self._ustawione_statki:
            self.trafiony(gra,inne[0],inne[1])
            self._trafione_statki.add((inne[0],inne[1]))
            self._temp_statki.add((inne[0],inne[1]))
            for x in self._temp_statki:
                (r,c)=x
                if self.czyplonie(r,c)==True:
                    break
            else:
                self.splonal(gra)
            if self.czykoniec(gra)==True:
                messagebox.showinfo("Koniec", "Wygrywa Użytkownik")
                gra.getFaza().next()
                return
        else:
            self.chybiony(gra,inne[0],inne[1])
            gra.StartSI()
    def Funkcja_faza3(self,gra,*inne):
        pass
    def Oznaczanie(self,gra):
        for x in self._ustawione_statki:
            if x not in self._trafione_statki:
                (r,c)=x
                self._pole = Button(gra.getgra(),height=2, width=5, bg="black")
                self._pole.grid(row=r, column=c)

                
class Plansza_User(Plansza):
    def __init__(self,gra):
        self._kolumna=0
        self._kolor="yellow"
        self.__stan=0
        self.__stan_statku="brak"
        self.pol(gra)
    def podswietl(self,gra,r,c):
        if gra.getFaza().getFaza()=="poczatek":
            if(self._statki_it>=self._statki.getlistlen()):
                pass
            else:
                (rzad2,kolumna2)=self.ustaw(r,c)
                if (r,c) in self._zakazane_pola or (rzad2,kolumna2) in self._zakazane_pola or rzad2>self._r_ogr or kolumna2>self._k_ogr:
                    pass
                else:
                    for i in range(r,rzad2+1):
                        for j in range(c,kolumna2+1):
                            self._pole = Button(gra.getgra(),height=2, width=5, bg="pink",command=lambda gra=gra, r=i, c=j:self.Funkcja_wybor(gra,r,c))
                            #self._pole.bind("<Enter>",lambda event,gra=gra, r=r, c=c:self.podswietl(gra,r,c))
                            self._pole.bind("<Leave>",lambda event,gra=gra, r=i, c=j:self.zgas(gra,r,c))
                            self._pole.bind("<Button-3>",lambda event,gra=gra,r=i,c=j:self.orient(gra,r,c))
                            self._pole.grid(row=i, column=j)
    def getStan(self):
        return self.__stan
    def warning(self,gra):
        if gra.getFaza().getFaza()=="poczatek":
            messagebox.showinfo("Złe ustawienie", "Na tym polu znajduje się już statek")
    def Funkcja_faza1(self,gra,*inne):
        if(self._statki_it>=self._statki.getlistlen()):
            self.__stan=1
            messagebox.showinfo("Statki ustawione", "Wszystkie statki zostały ustawione, wciśnij przycisk Nowa Gra, aby rozpocząć grę")
        else:
            (rzad2,kolumna2)=self.ustaw(inne[0],inne[1])
            if (inne[0],inne[1]) in self._zakazane_pola or (rzad2,kolumna2) in self._zakazane_pola or rzad2>self._r_ogr or kolumna2>self._k_ogr:
                messagebox.showinfo("Złe ustawienie", "Statki muszą całe znajdować się na planszy i nie stykać się bokami/rogami")
            else:
                for i in range(inne[0]-1,rzad2+2):
                    for j in range(inne[1]-1,kolumna2+2):
                        self._zakazane_pola.add((i,j))
                for i in range(inne[0],rzad2+1):
                    for j in range(inne[1],kolumna2+1):
                        self._pole = Button(gra.getgra(),height=2, width=5, bg="purple", command=lambda: self.warning(gra))
                        self._pole.grid(row=i, column=j)
                        self._ustawione_statki.add((i,j))
                self._statki_it=self._statki_it+1
                if(self._statki_it==self._statki.getlistlen()):
                    self.__stan=1
                    self._zakazane_pola=set()
    def Funkcja_faza2(self,gra,*inne):
        pass
    def Funkcja_faza3(self,gra,*inne):
        pass
    def ustaw(self,r,c):
        if self._orientacja=="pionowa":
            r=r+self._statki.getStatki()[self._statki_it]-1
        elif self._orientacja=="pozioma":
            c=c+self._statki.getStatki()[self._statki_it]-1
        return (r,c)
    def zgas(self,gra,r,c):
        if gra.getFaza().getFaza()=="poczatek":
            if(self._statki_it>=self._statki.getlistlen()):
                pass
            else:
                (rzad2,kolumna2)=self.ustaw(r,c)
                if (r,c) in self._zakazane_pola or (rzad2,kolumna2) in self._zakazane_pola or rzad2>self._r_ogr or kolumna2>self._k_ogr:
                    pass
                else:
                    for i in range(r,rzad2+1):
                        for j in range(c,kolumna2+1):
                            self._pole = Button(gra.getgra(),height=2, width=5, bg=self._kolor,command=lambda gra=gra, r=i, c=j:self.Funkcja_wybor(gra,r,c))
                            self._pole.bind("<Enter>",lambda event,gra=gra, r=i, c=j:self.podswietl(gra,r,c))
                            #self._pole.bind("<Leave>",lambda event,gra=gra, r=r, c=c:self.zgas(gra,r,c))
                            self._pole.bind("<Button-3>",lambda event,gra=gra,r=i,c=j:self.orient(gra,r,c))
                            self._pole.grid(row=i, column=j)
    def orient(self,gra,r,c):
        if self._orientacja=="pionowa":
            self.zgas(gra,r,c)
            self._orientacja="pozioma"
            self.podswietl(gra,r,c)
        elif self._orientacja=="pozioma":
            self.zgas(gra,r,c)
            self._orientacja="pionowa"
            self.podswietl(gra,r,c)
    def Strzelanie(self,gra):
        if self.__stan_statku=="plonie":
            self.sprawdz_pola()
            it=len(self._pola_okol)-1
            ran=rand.randint(0,it)
            it=0
            for x in self._pola_okol:
                if ran==it:
                    (r,c)=x
                it=it+1
        else:
            r=rand.randint(self._rzad,self._r_ogr)
            c=rand.randint(self._kolumna,self._k_ogr)
            while (r,c) in self._zakazane_pola:
                r=rand.randint(self._rzad,self._r_ogr)
                c=rand.randint(self._kolumna,self._k_ogr)
        self._zakazane_pola.add((r,c))
        if (r,c) in self._ustawione_statki:
            self.trafiony(gra,r,c)
            self._trafione_statki.add((r,c))
            self._temp_statki.add((r,c))
            self._zakazane_pola.add((r+1,c+1))
            self._zakazane_pola.add((r-1,c+1))
            self._zakazane_pola.add((r+1,c-1))
            self._zakazane_pola.add((r-1,c-1))
            for x in self._temp_statki:
                (r,c)=x
                if self.czyplonie(r,c)==True:
                    self.okolica(r,c)
                    self.__stan_statku="plonie"
                    break
            else:
                self.splonal(gra)
                self._pola_okol=set()
                self.__stan_statku="splonal"
            if self.czykoniec(gra)==True:
                messagebox.showinfo("Koniec", "Wygrywa SI")
                gra.getFaza().next()
                gra.Porazka()
                return
            else:
                self.Strzelanie(gra)
        else:
            self.chybiony(gra,r,c)

            
class Plansza_kolekcja(object):
    def __init__(self,gra):
        self.__PU=Plansza_User(gra)
        self.__PSI=Plansza_SI(gra)
    def getStan(self):
        return self.__PU.getStan()
    def StatkiSI(self):
        self.__PSI.ustawianie()
    def StartSI(self,gra):
        self.__PU.Strzelanie(gra)
    def Porazka(self,gra):
        self.__PSI.Oznaczanie(gra)

        
class Przycisk_kolekcja(object):
    def __init__(self,gra):
        self.__New_Game=Nowa_Gra(gra)
        self.__Res=Reset(gra)

        
class Faza(object):
    def __init__(self):
        self.__lista_faz=["poczatek","gra","koniec"]
        self.__obecna_faza=self.__lista_faz[0]
        self.__iterator=0
    def getFaza(self):
        return self.__obecna_faza
    def next(self):
        if self.__iterator<len(self.__lista_faz)-1:
            self.__iterator=self.__iterator+1
            self.__obecna_faza=self.__lista_faz[self.__iterator]

            
class Statki(object):
    def __init__(self):
        self.__lista_statkow=[4,3,3,2,2,2,1,1,1,1]
    def getStatki(self):
        return self.__lista_statkow
    def getlistlen(self):
        return len(self.__lista_statkow)

    
class Main(object):
    def __init__(self):
        self.__gra = Tk()
        self.__gra.title('Statki')
        messagebox.showinfo("Gra w statki", "Witaj w grze Statki! Twoim celem jest pokonanie sztucznej inteligencji. Najpierw musisz rozstawić swoje statki na żółtej planszy, a następnie kliknąć przycisk Nowa Gra, potem probujesz trafic statki na planszy wroga - zielonej. Wygrywa ten, kto pierwszy spali wszystkie statki wroga!")
        self.__faza_roz=Faza()
        self.__przycisk=Przycisk_kolekcja(self)
        self.__plansze=Plansza_kolekcja(self)
        self.__gra.mainloop()
    def getFaza(self):
        return self.__faza_roz
    def getStan(self):
        return self.__plansze.getStan()
    def Reset(self):
        self.__faza_roz=Faza()
        self.__przycisk=Przycisk_kolekcja(self)
        self.__plansze=Plansza_kolekcja(self)
    def StatkiSI(self):
        self.__plansze.StatkiSI()
    def StartSI(self):
        self.__plansze.StartSI(self)
    def Porazka(self):
        self.__plansze.Porazka(self)
    def getgra(self):
        return self.__gra
#game=Main()

